
# Also check out this discussion of ragged arrays:
# http://tonysyu.github.io/ragged-arrays.html#.VPP0zfnF98E

import numpy as np
import pandas as pd
import os

datadir = os.path.join(os.environ.get('KAGGLE_DATA'), 'rain')

colnames = ['Id', 'TotalRain', 'RadarNumber','TimeToEnd', 'DistanceToRadar', 'Composite', 'HybridScan',
          'HydrometeorType', 'Kdp', 'RR1', 'RR2', 'RR3', 'RadarQualityIndex',
          'Reflectivity', 'ReflectivityQC', 'RhoHV', 'Velocity', 'Zdr',
          'LogWaterVolume', 'MassWeightedMean', 'MassWeightedSD']

shortcolnames = ['Id', 'TR', 'RN','Time', 'Dist', 'Comp', 'HS',
          'HT', 'Kdp', 'RR1', 'RR2', 'RR3', 'RQI',
          'Ref', 'RefQC', 'RhoHV', 'Vel', 'Zdr',
          'LWV', 'MWM', 'MWSD']

def bin_read_random(binfile, aux, points, nprint=10000, dataframe=False):

    alldata = []
    m = 18

    npoints = aux[0]
    offsets = aux[1]
    for i in points:
        binfile.seek(offsets[i], 0)
        n = npoints[i]
        data = np.fromfile(binfile, dtype=np.float32, count=n*m).reshape(n,m)
        if dataframe:
            data = pd.DataFrame(data=data,columns=columns)
        alldata += [data]
        if not i%nprint:
             print 'Loading: ', i
    return alldata

def bin_read_all_data(binfile, aux, nprint=10000, dataframe=False):
    points = range(len(aux[0]))
    return bin_read_random(binfile, aux, points, nprint=nprint, dataframe=dataframe)

def read_aux(path):
    # usage: npoints, offsets, actuals = read_aux(path)
    aux = np.load(path)
    return aux[:,0], aux[:,1], aux[:,2]

if __name__ == '__main__':

    basename = 'med'

    binpath = os.path.join(datadir, basename+'.bin')
    auxpath = os.path.join(datadir, basename+'-aux.npy')


    aux = read_aux(auxpath)
    with open(binpath, 'rb') as binfile:
        #alldata = bin_read_all_data(binfile, aux)
        alldata = bin_read_random(binfile, aux, range(10), dataframe=True)






