import csv
import numpy as np
from os.path import join
from rain import datadir


def csv2bin(basename):

    maxlen = 2000000
    npoints = np.empty(maxlen, dtype=np.int32)
    expect = np.empty_like(npoints)

    csvpath = join(datadir, basename+'.csv')
    binpath = join(datadir, basename+'.bin')
    auxpath = join(datadir, basename+'-aux.npy')


    with open(csvpath, 'rb') as csvfile, \
        open(binpath, 'wb') as binfile:

            reader = csv.reader(csvfile, delimiter=',')
            header = reader.next()

            for i, row in enumerate(reader):
                sample = [x.split() for x in row]
                index = data = np.array(sample[1]).astype(np.float32)
                data = np.array(sample[1:19]).astype(np.float32).transpose()
                if len(sample) == 20:
                    expect[i] = float(sample[19][0])
                else:
                    expect[i] = -99900
                npoints[i] = data.shape[0]
                data.tofile(binfile)
                if not i%10000:
                    print "Row: ", i

    with open(auxpath, 'wb') as auxfile:
        npoints = npoints[0:i+1]
        expect = expect[0:i+1]
        offset = np.roll((npoints*4*18),1)
        offset[0] = 0
        offset = offset.cumsum()
        aux = np.empty((len(npoints), 3), dtype=np.int32)
        aux[:,0] = npoints
        aux[:,1] = offset
        aux[:,2] = expect
        np.save(auxfile,aux)

for basename in ['train_2013', 'test_2014']:

    print 'Processing {}'.format(basename)
    csv2bin(basename)


