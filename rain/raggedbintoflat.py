# -*- coding: utf-8 -*-
"""
Created on Mon Mar 02 20:20:35 2015

@author: gregas
"""

import rain as rn
import numpy as np
from os.path import join, getsize

basename = 'train_2013'

def bin2flat(basename):

    binpath = join(rn.datadir, basename+'.bin')
    auxpath = join(rn.datadir, basename+'-aux.npy')
    npypath = join(rn.datadir, basename+'.npy')

    with open(binpath, 'rb') as binfile:

        nmeas = getsize(binpath)/(18*4)
        meas = 0
        aux = rn.read_aux(auxpath)
        nexamples = len(aux[0])
        tr = aux[2]
        alldata = np.empty((nmeas,21), dtype=np.float32)
        for exid in range(nexamples): # for every example
            if not exid % 10000:
                print 'Example #:', exid
            example = rn.bin_read_random(binfile, aux, [exid], dataframe=False)[0]
            radnum = 1
            for m in range(1, example.shape[0]):
                if example[m,0] > example[m-1,0]: # if time to hour goes up
                    radnum += 1                   # its a different radar
                alldata[meas, 0] = exid+1 # id starting at 1
                alldata[meas, 1] = tr[exid] # total rain for that example
                alldata[meas, 2] = radnum # radar number
                alldata[meas, 3:] = example[m,:] # the rest of the data
                meas += 1
        alldata = alldata[1:,:]
        np.save(npypath,alldata)

for basename in ['train_2013', 'test_2014']:

    print 'Processing {}'.format(basename)
    bin2flat(basename)











