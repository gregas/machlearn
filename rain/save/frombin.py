import numpy as np
from struct import unpack

basename = 'test_2014'

m = 18
i = 0
with open(basename+'.bin', 'rb') as binfile:

    str = binfile.read(4)
    while str:
        n = unpack('<L', str)[0]
        data = np.fromfile(binfile, dtype=np.float32, count=n*m).reshape(n,m)
        #if not i%1:
        #    print i, n, len(data)
        #i += 1
        str = binfile.read(4)