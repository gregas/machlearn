import csv
import numpy as np
from struct import pack

header = ['Id', 'TimeToEnd', 'DistanceToRadar', 'Composite', 'HybridScan',
          'HydrometeorType', 'Kdp', 'RR1', 'RR2', 'RR3', 'RadarQualityIndex',
          'Reflectivity', 'ReflectivityQC', 'RhoHV', 'Velocity', 'Zdr',
          'LogWaterVolume', 'MassWeightedMean', 'MassWeightedSD', 'Expected']


basename = 'test_2014'

with open(basename+'.csv', 'rb') as csvfile, \
    open(basename+'.bin', 'wb') as binfile, \
    open(basename+'.aux', 'wb') as auxfile:

        reader = csv.reader(csvfile, delimiter=',')
        header = reader.next()
        aux = {}
        aux['header'] = header
        for i, row in enumerate(reader):
            sample = [x.split() for x in row]
            #expct = float(sample[19][0])
            index = data = np.array(sample[1]).astype(np.float32)
            data = np.array(sample[1:19]).astype(np.float32).transpose()
            binfile.write(pack('<L', data.shape[0]))
            data.tofile(binfile)
            if not i % 10000:
                print "Row: ", i
