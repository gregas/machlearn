import csv
import pandas as pd
import numpy as np
import pickle

header = ['Id', 'TimeToEnd', 'DistanceToRadar', 'Composite', 'HybridScan',
          'HydrometeorType', 'Kdp', 'RR1', 'RR2', 'RR3', 'RadarQualityIndex',
          'Reflectivity', 'ReflectivityQC', 'RhoHV', 'Velocity', 'Zdr',
          'LogWaterVolume', 'MassWeightedMean', 'MassWeightedSD', 'Expected']


basename = 'med'


with open(basename+'.npy', 'rb') as npfile:
    x = np.load(npfile)




