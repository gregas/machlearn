import csv
import pandas as pd
import numpy as np

basename = 'med'

with open(basename+'.csv', 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    header = reader.next()
    hdf = pd.HDFStore(basename+'.h5')
    for i, row in enumerate(reader):
        sample = [x.split() for x in row]
        snum = float(sample[0][0])
        expct = float(sample[19][0])
        index = data = np.array(sample[1]).astype(np.float32)
        data = np.array(sample[1:19]).astype(np.float32).transpose()

        df = pd.DataFrame(index=index,data=data,columns=header[1:-1])
        df.set_index('TimeToEnd')

        key = 's'+sample[0][0]
        hdf.put(key, df)
        if not i % 1000:
            print "Row: ", i

hdf.close()


