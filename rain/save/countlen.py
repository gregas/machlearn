import csv
import numpy as np
import h5py as h5


ml=0
with open('test_2014.csv', 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    header = reader.next()
    for i, row in enumerate(reader):
        sample = [x.split() for x in row]
        l = len(sample[1])
        if l > ml:
            ml = l
            print 'ml', ml
        if not i % 10000: print 'row / ml', i, ml
print 'total rows', i