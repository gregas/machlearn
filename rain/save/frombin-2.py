import numpy as np

aux = np.load(datadir+basename+'-aux.npy')
npts = aux.shape[0]

alldata = []
with open(datadir+basename+'.bin', 'rb') as binfile:

    for i in range(npts):
        n = aux[i,0]
        data = np.fromfile(binfile, dtype=np.float32, count=n*m).reshape(n,m)
        alldata += [data]
        if not i%10000:
             print 'Loading: ', i

#    binfile.seek(aux[x,1], 0)
#    n = aux[x,0]
#    data = np.fromfile(binfile, dtype=np.float32, count=n*m).reshape(n,m)
#    print x, aux[x,0], aux[x,1], data


def read_aux(path):
    return np.load(path)

datadir = '../data/'
basename = 'train_2013'

m = 18


