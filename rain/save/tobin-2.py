import csv
import numpy as np


header = ['Id', 'TimeToEnd', 'DistanceToRadar', 'Composite', 'HybridScan',
          'HydrometeorType', 'Kdp', 'RR1', 'RR2', 'RR3', 'RadarQualityIndex',
          'Reflectivity', 'ReflectivityQC', 'RhoHV', 'Velocity', 'Zdr',
          'LogWaterVolume', 'MassWeightedMean', 'MassWeightedSD', 'Expected']


basename = '../data/train_2013'
maxlen = 2000000
npoints = np.empty(maxlen, dtype=np.int32)
expect = np.empty_like(npoints)

with open(basename+'.csv', 'rb') as csvfile, \
    open(basename+'.bin', 'wb') as binfile:

        reader = csv.reader(csvfile, delimiter=',')
        header = reader.next()

        for i, row in enumerate(reader):
            sample = [x.split() for x in row]
            index = data = np.array(sample[1]).astype(np.float32)
            data = np.array(sample[1:19]).astype(np.float32).transpose()
            if len(sample) == 20:
                expect[i] = float(sample[19][0])
            npoints[i] = data.shape[0]
            data.tofile(binfile)
            if not i%10000:
                print "Row: ", i

with open(basename+'-aux.npy', 'wb') as auxfile:
    npoints = npoints[0:i+1]
    expect = expect[0:i+1]
    offset = np.roll((npoints*4*18),1)
    offset[0] = 0
    offset = offset.cumsum()
    aux = np.empty((len(npoints), 3), dtype=np.int32)
    aux[:,0] = npoints
    aux[:,1] = offset
    aux[:,2] = expect
    np.save(auxfile,aux)
