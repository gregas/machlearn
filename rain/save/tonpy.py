import csv
import pandas as pd
import numpy as np
import pickle

header = ['Id', 'TimeToEnd', 'DistanceToRadar', 'Composite', 'HybridScan',
          'HydrometeorType', 'Kdp', 'RR1', 'RR2', 'RR3', 'RadarQualityIndex',
          'Reflectivity', 'ReflectivityQC', 'RhoHV', 'Velocity', 'Zdr',
          'LogWaterVolume', 'MassWeightedMean', 'MassWeightedSD', 'Expected']


basename = 'med'

with open(basename+'.csv', 'rb') as csvfile, \
    open(basename+'.npy', 'wb') as npfile, \
    open(basename+'.aux', 'wb') as auxfile:

        reader = csv.reader(csvfile, delimiter=',')
        header = reader.next()
        aux = {}
        aux['header'] = header
        for i, row in enumerate(reader):
            sample = [x.split() for x in row]
            expct = float(sample[19][0])
            index = data = np.array(sample[1]).astype(np.float32)
            data = np.array(sample[1:19]).astype(np.float32).transpose()
            #data.tofile(npfile)
            np.save(npfile, data)
            if not i % 1000:
                print "Row: ", i

