import numpy as np
import crps

solfile = 'trainSolution.csv'
auxfile = '../data/train_2013-aux.npy'

predictions = np.loadtxt(solfile, delimiter=',',skiprows=1)[:,1:]
thresholds = np.arange(70, dtype=np.float32)
aux = np.load(auxfile)
actuals = aux[:,2]

crps = crps.calc_crps(thresholds, predictions, actuals)

