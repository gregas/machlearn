# -*- coding: utf-8 -*-
"""
Created on Mon Mar 02 20:20:35 2015

@author: gregas
"""
# This does not work very well...files are huge!!!

import rain as rn
import numpy as np
import pandas as pd
import os

basename = 'med'


npypath = os.path.join(rn.datadir, basename+'.npy')
h5path = os.path.join(rn.datadir, basename+'.h5')


npd = np.load(npypath)
df = pd.DataFrame(data=npd, columns=rn.shortcolnames)

h5file = pd.HDFStore(h5path)

for i in range(df.shape[0]):


#    label = str(int(df.ix[i,'Id'])) +'/' + \
#            str(int(df.ix[i,'RadNum'])) +'/'+ \
#            str(int(df.ix[i, 'Time']))

    label = 'Id{}/RadNum{}/Time{}'.format(int(df.ix[i,'Id']),
        int(df.ix[i,'RadNum']), int(df.ix[i,'Time']))
    h5file[label] = df.ix[i,'Dist':]
    if not i%1000: print i, label









